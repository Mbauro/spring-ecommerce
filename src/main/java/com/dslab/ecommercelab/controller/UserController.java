package com.dslab.ecommercelab.controller;

import com.dslab.ecommercelab.entity.User;
import com.dslab.ecommercelab.interfaces.UserRepository;
import com.dslab.ecommercelab.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    UserService userService;

    //POST http://localhost:8080/user/register
    @PostMapping(path = "register")
    public @ResponseBody User register(@RequestBody User user) {
        return userService.addUser(user);

    }
    //POST http://localhost:8080/user/registerAdmin
    @PostMapping(path="registerAdmin")
    public @ResponseBody User registerAdmin(@RequestBody User user){
        return userService.addAdmin(user);
    }

    //GET http://localhost:8080/user/all
    @GetMapping(path = "/all")
    public @ResponseBody Iterable<User> getAll() {

        return userService.getAllUser();
    }

    //GET http://localhost:8080/user/1
    @GetMapping(path = "/{id}")
    public @ResponseBody User getUser(@PathVariable Integer id) {
        return userService.getUserById(id);
    }

    //GET http://localhost:8080/user/email/{email}
    @GetMapping(path="/email/{email}")
    public @ResponseBody User getUserByEmail(@PathVariable String email){
        return userService.getUserByEmail(email);
    }

    //DELETE http://localhost:8080/user/1
    @DeleteMapping(path = "/{id}")
    public @ResponseBody String deleteUser (@PathVariable Integer id) {
        return userService.deleteUser(id);
    }
}
