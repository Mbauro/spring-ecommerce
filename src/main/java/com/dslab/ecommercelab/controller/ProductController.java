package com.dslab.ecommercelab.controller;

import com.dslab.ecommercelab.entity.Product;
import com.dslab.ecommercelab.interfaces.ProductRepository;
import com.dslab.ecommercelab.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
//Tutte le richieste che avranno come path == product
//verranno gestite da questo controller
@RequestMapping(path="product")
public class ProductController {

    @Autowired
    ProductService service;

    @PostMapping(path="/add")
    public @ResponseBody Product addProduct(@RequestBody Product product){

        return service.addProduct(product);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Product> getAll(){

        return service.getAll();
    }

    @GetMapping(path="/{id}")
    public @ResponseBody
    Optional<Product> getProductById(@PathVariable Integer id){
        return service.getProductById(id);
    }

    @DeleteMapping(path="/{id}")
    public @ResponseBody
    String deleteProduct(@PathVariable Integer id){
        return service.deleteProduct(id);

    }
}

