package com.dslab.ecommercelab.controller;

import com.dslab.ecommercelab.entity.FinalOrder;
import com.dslab.ecommercelab.entity.OrderProduct;
import com.dslab.ecommercelab.entity.Product;
import com.dslab.ecommercelab.entity.User;
import com.dslab.ecommercelab.interfaces.OrderProductRepository;
import com.dslab.ecommercelab.interfaces.OrderRepository;
import com.dslab.ecommercelab.interfaces.ProductRepository;
import com.dslab.ecommercelab.interfaces.UserRepository;
import com.dslab.ecommercelab.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping(path = "order")

public class OrderController {

    @Autowired
    OrderService service;

    @GetMapping(path = "/all")
    public @ResponseBody Iterable <FinalOrder> getAllOrders(){
        return service.getAllOrders();
    }

    @PostMapping(path ="/insert")
    public @ResponseBody String insertOrder(@RequestBody OrderRequest orderRequest){
        return service.insertOrder(orderRequest);
    }

}