package com.dslab.ecommercelab.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/*
 * L'ordine deve essere associato ad un utente e deve avere una
 * lista di Prodotti. Creiamo un'altra classe ProdottoOrdine
 * che ci serve come appoggio. La lista di prodotti sarà quindi
 * una lista di ProdottoOrdine.
 * La relazione tra Ordine e User è ManyToOne
 * La relazione tra Ordine e List<ProdottoOrdine> è OneToMany
 *
 * */
@Entity
public class FinalOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    private User user;

    //Propaga le operazioni di persistenza
    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderProduct> products;

    private String state;

    public FinalOrder() {
        this.products = new ArrayList<OrderProduct>();
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<OrderProduct> getProducts() {
        return products;
    }

    public void setProducts(List<OrderProduct> products) {
        this.products = products;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getTotal(){
        double total = 0.0;
        for(OrderProduct product : products){
            total += product.getTotal();
        }
        return total;
    }

}
