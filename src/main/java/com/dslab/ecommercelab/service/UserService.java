package com.dslab.ecommercelab.service;

import com.dslab.ecommercelab.entity.User;
import com.dslab.ecommercelab.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    UserRepository repository;


    public User addUser(User user){
        user.setRoles(Collections.singletonList("USER"));
        return repository.save(user);
    }

    public User addAdmin(User user){
        user.setRoles(Collections.singletonList("ADMIN"));
        return repository.save(user);
    }

    public Iterable<User> getAllUser(){
        return repository.findAll();
    }

    public User getUserById(Integer id){
        Optional<User> byId = repository.findById(id);
        return byId.get();
    }

    public User getUserByEmail(String email){
        Optional<User> byEmail = repository.findByEmail(email);
        return byEmail.get();
    }

    public String deleteUser(Integer id){
        repository.deleteById(id);
        return String.format("The user with id %d has been deleted!", id);
    }
}
