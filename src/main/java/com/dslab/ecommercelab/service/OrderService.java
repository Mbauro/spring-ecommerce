package com.dslab.ecommercelab.service;


import com.dslab.ecommercelab.controller.OrderRequest;
import com.dslab.ecommercelab.entity.FinalOrder;
import com.dslab.ecommercelab.entity.OrderProduct;
import com.dslab.ecommercelab.entity.Product;
import com.dslab.ecommercelab.entity.User;
import com.dslab.ecommercelab.interfaces.OrderProductRepository;
import com.dslab.ecommercelab.interfaces.OrderRepository;
import com.dslab.ecommercelab.interfaces.ProductRepository;
import com.dslab.ecommercelab.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Order;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderProductRepository orderProductRepository;

    public Iterable<FinalOrder> getAllOrders(){
        return orderRepository.findAll();
    }

    public String insertOrder(OrderRequest orderRequest){
        //Get user from the order request
        Optional<User> u = userRepository.findById(orderRequest.getUserId());
        try{
            User user = u.get();
            //Extract product id from Map
            Object keys[] = orderRequest.getProduct().keySet().toArray();
            int product_id[] = new int [keys.length];

            for(int i = 0; i < keys.length; i++){
                product_id[i] = (Integer)orderRequest.getProduct().keySet().toArray()[i];
                System.out.println("Product id: "+product_id[i]);
            }
            FinalOrder finalOrder = new FinalOrder();
            for(int i = 0; i < product_id.length; i++) {
                Optional<Product> p = productRepository.findById(product_id[i]);
                Product product = p.get();
                //Get quantity to order from the map
                int quantity = orderRequest.getProduct().get(product_id[i]);
                System.out.println("quantity: "+quantity);
                System.out.println("Prod quantity: "+product.getItems());
                if (quantity < product.getItems()) {

                    OrderProduct orderProduct = new OrderProduct();
                    orderProduct.setQuantity(quantity);
                    orderProduct.setProduct(product);
                    finalOrder.setUser(user);
                    finalOrder.getProducts().add(orderProduct);
                    orderProductRepository.save(orderProduct);
                    //Update product quantity in stock
                    product.setItems(product.getItems()-quantity);
                    productRepository.save(product);

                }
                else {
                    return ("Selected quantity not available");
                }
            }
            orderRepository.save(finalOrder);
            return ("Final Order inserted into database");

        }catch(NoSuchElementException e){
            return("Cant find user or product");
        }
    }
}
