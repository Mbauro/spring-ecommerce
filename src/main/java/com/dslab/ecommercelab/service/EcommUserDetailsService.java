package com.dslab.ecommercelab.service;

import com.dslab.ecommercelab.entity.User;
import com.dslab.ecommercelab.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional

public class EcommUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException{
        Optional<User> byEmail = repository.findByEmail(email);
        User user = byEmail.get();
        if(user == null){
            throw  new UsernameNotFoundException("User not found!");
        }
        else {
            return new org.springframework.security.core.userdetails.User(
                    user.getEmail(),
                    getEncoder().encode(user.getPassword()),
                    true,
                    true,
                    true,
                    true,
                    getAuth(user.getRoles())
            );
        }
    }

    @Bean
    public BCryptPasswordEncoder getEncoder(){
        return new BCryptPasswordEncoder();
    }

    public List<GrantedAuthority> getAuth(List<String> roles){
        List<GrantedAuthority> authorities = new ArrayList<>();

        for(final String role: roles) {
            authorities.add(new SimpleGrantedAuthority((role)));

        }
        return authorities;
    }
}
