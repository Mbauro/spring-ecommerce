package com.dslab.ecommercelab.service;

import com.dslab.ecommercelab.entity.Product;
import com.dslab.ecommercelab.interfaces.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
@Transactional
public class ProductService {

    @Autowired
    ProductRepository repository;

    public Product addProduct(Product product){
        return repository.save(product);
    }

    public Iterable<Product> getAll(){
        return repository.findAll();
    }

    public Optional<Product> getProductById(Integer id){
        Optional byId = repository.findById(id);
        return byId;
    }

    public String deleteProduct(Integer id){
        repository.deleteById(id);
        return "The product with id "+id+" has been deleted!";
    }
}
