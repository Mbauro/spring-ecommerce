package com.dslab.ecommercelab.configuration;



import com.dslab.ecommercelab.service.EcommUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class EcommConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    EcommUserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.httpBasic().and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/user/register").permitAll()
                .antMatchers("/user/all").hasAuthority("ADMIN")
                .antMatchers("/user/email/{email}").hasAnyAuthority("ADMIN,USER")
                .antMatchers(HttpMethod.GET,"/user/{id}").hasAnyAuthority("ADMIN,USER")
                .antMatchers(HttpMethod.DELETE,"/user/{id}").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST,"/user/registerAdmin").hasAuthority("ADMIN")
                /*Handle requests for products*/
                .antMatchers("/product/add").hasAuthority("ADMIN")
                .antMatchers("/product/all").hasAnyAuthority("ADMIN,USER")
                .antMatchers(HttpMethod.GET,"/product/{id}").hasAnyAuthority("ADMIN,USER")
                .antMatchers(HttpMethod.DELETE,"/product/{id}").hasAuthority("ADMIN")
                /*Handles order requests*/
                .antMatchers("/order/all").hasAuthority("ADMIN")
                .antMatchers("order/insert").hasAnyAuthority("ADMIN,USER")
                .anyRequest().authenticated()
                .and()
                .csrf().disable();

    }
}
