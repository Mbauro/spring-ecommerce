package com.dslab.ecommercelab.interfaces;

import com.dslab.ecommercelab.entity.OrderProduct;
import org.springframework.data.repository.CrudRepository;

public interface OrderProductRepository extends CrudRepository<OrderProduct,Integer> {
}