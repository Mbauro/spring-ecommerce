package com.dslab.ecommercelab.interfaces;

import com.dslab.ecommercelab.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

//CrudRepository<T,ID>
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByEmail(String email);
}
