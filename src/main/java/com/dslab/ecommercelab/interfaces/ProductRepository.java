package com.dslab.ecommercelab.interfaces;


import com.dslab.ecommercelab.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product,Integer> {



}
