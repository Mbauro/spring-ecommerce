package com.dslab.ecommercelab.interfaces;

import com.dslab.ecommercelab.entity.FinalOrder;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<FinalOrder,Integer> {
}